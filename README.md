# Whatis

`lfb-docx2html.py` is a python script that converts docx[^1] file to html.

It makes use of [pypandoc](https://pypi.org/project/pypandoc/) or [python-mammoth](https://github.com/mwilliamson/python-mammoth).

After a simple conversion some tags are wrapped or expanded by [BeautifulSoup](https://www.crummy.com/software/BeautifulSoup/bs4/doc/) according to our needs.

# Prerequisites

You need a working python installation with these packages (`pip3 install foobarbaz`)

- pypandoc
- mammoth
- beautifulsoup4

In addition you need to have pandoc installed in your operating system. 
You may want to adjust the path to the pandoc binary in the script (`os.environ.setdefault('PYPANDOC_PANDOC', '/path/to/pandoc')`)

# Howto

## Full (oversized) Version
Run the script with these parameters: 

`-c` : converter, values 'pandoc' (default) | 'mammoth\
`-i` : input-filename mandatory\
`-o` : output-filename - falling back to output.html 

e.g. `python lfb-docx2html.py -c pandoc -i input.docx -o output.html`

minimal: `python lfb-docx2html.py -i input.docx`

## Simple Version
After it became clear that pandoc gives better results, a simple version of the script without mammoth etc. was developed.

The file `lfb-simple-docx2html.py` is executed without parameters.

`python3 ./lfb-simple-docx2html.py "path/to/file.docx"`


<hr> 
[^1]: mammoth only converts docx files. If you use pandoc, you can also use other office formats like OpenDocument Text files