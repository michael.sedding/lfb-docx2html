import os, sys, shutil, re, getopt
import pypandoc
from bs4 import BeautifulSoup
os.environ.setdefault('PYPANDOC_PANDOC', '/usr/bin/pandoc')


converter = 'pandoc'
input_filename = sys.argv[1]
basename = input_filename.split(".")[0]
output_filename = basename + "-pandoc.html"
output_dir = "./" + basename + "-pandoc/" # with trailing slash


print('Converting ' + input_filename + ' to ' + output_filename + ' with ' + converter)


def convertPandoc():
    global html
    html = pypandoc.convert_file(input_filename, 'html5', extra_args=['--extract-media=.'])


def beautifyHtml():
    ''' Use BeautifulSoup to prettify html. '''
    global html
    html = BeautifulSoup(html, 'html.parser')
    html = html.prettify()
    #print(html)

def cookSoup():
    ''' Use BeautifulSoup to do some magic. '''
    global html
    html = BeautifulSoup(html, 'html.parser')
    # add class=extern, target=_blank to weblinks
    for hyperlink in html.findAll('a', href=re.compile("https?://")):
        hyperlink['class']='extern'
        hyperlink['target']='_blank'
    # wrap table-tags in div.table-responsive
    for table in html.findAll('table'):
        wrapper = html.new_tag('div', **{"class": "table-responsive"})
        table.wrap(wrapper)
    # extend img-tags with attribute class=img-responsive
    img_count=1
    for image in html.findAll('img'):
        image['class']='img-responsive'
        image['alt']="Bild "+ str(img_count) + " aus " + input_filename
        img_count+=1
    # append caption-line after image
    for image in html.findAll('img'):
        caption = html.new_tag('p', **{"class": "caption small text-right"})
        caption.append("Abbildung: Quellenangabe")
        image.insert_after(caption)
    # delete all appearances of blockquote, since they are almost always just used for pointless indentation
    for bq in html.findAll('blockquote'):
        bq.unwrap()

    html = html.prettify()



def writeOutput():
    ''' Save the html to a file. '''
    global output_filename
    with open(output_filename, "w") as f:
        global html
        f.writelines(html)


#### Main
convertPandoc()
cookSoup()
writeOutput()
