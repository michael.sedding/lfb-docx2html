import os, sys, shutil, re, getopt
import mammoth
import pypandoc
from bs4 import BeautifulSoup
os.environ.setdefault('PYPANDOC_PANDOC', '/usr/bin/pandoc')

converter = 'pandoc'
#for i in os.listdir('./'):
#    if i.endswith(".docx"):
#        print("first docx-files in Folder is",i)
#        input_filename = i

input_filename = "file-sample_100kB.docx"
output_filename = "output.html"
output_dir = "./pix/" # with trailing slash
styles_filename = "lfb-styles.txt" #used by mammoth

argv = sys.argv[1:]
# optional commandline-parameters:
# -c : converter, values 'pandoc' (default) | 'mammoth 
# -i : input-filename - mandatory parameter
# -o : output-filename - falling back to output.html 
try:
    opts, args = getopt.getopt(argv, 'c:i:o:')
except:
    print("Error")

for opt, arg in opts:
    if opt in ['-c']:
        converter = arg
    elif opt in ['-i']:
        input_filename = arg
    elif opt in ['-o']:
        output_filename = arg 

print('Converting ' + input_filename + ' to ' + output_filename + ' with ' + converter)



class ImageWriter(object):
    def __init__(self, output_dir):
        self._output_dir = output_dir
        self._image_number = 1

    def __call__(self, element):
        extension = element.content_type.partition("/")[2]
        image_filename = "{0}.{1}".format(self._image_number, extension)
        with open(os.path.join(self._output_dir, image_filename), "wb") as image_dest:
            with element.open() as image_source:
                shutil.copyfileobj(image_source, image_dest)

        self._image_number += 1

        return {"src": self._output_dir+image_filename}


convert_image = mammoth.images.inline(ImageWriter(output_dir))


def readStyles():
    ''' Read the list of parsing-rules from a file an save it to custom_styles. '''
    with open(styles_filename) as styles_file:
        global custom_styles
        custom_styles = styles_file.read()
        #print(custom_styles)

def convertMammoth():
    ''' Read the content of an docx-file and convert it to html using mammoth. '''
    with open(input_filename, "rb") as docx_file:
        result = mammoth.convert_to_html(docx_file, style_map = custom_styles, convert_image=convert_image)
        global html
        html = result.value

def convertPandoc():
    global html
    html = pypandoc.convert_file(input_filename, 'html5', extra_args=['--extract-media=.'])


def beautifyHtml():
    ''' Use BeautifulSoup to prettify html. '''
    global html
    html = BeautifulSoup(html, 'html.parser')
    html = html.prettify()
    #print(html)

def cookSoup():
    ''' Use BeautifulSoup to do some magic. '''
    global html
    html = BeautifulSoup(html, 'html.parser')
    # wrap table-tags in div.table-responsive
    for table in html.findAll('table'):
        wrapper = html.new_tag('div', **{"class": "table-responsive"})
        table.wrap(wrapper)
    # extend img-tags with attribute class=img-responsive
    img_count=1
    for image in html.findAll('img'):
        image['class']='img-responsive'
        image['alt']="Bild "+ str(img_count) + " aus " + input_filename
        img_count+=1
    # add class=extern, target=_blank to weblinks
    for hyperlink in html.findAll('a', href=re.compile("https?://")):
        hyperlink['class']='extern'
        hyperlink['target']='_blank'

        
    html = html.prettify()
    


def writeOutput():
    ''' Save the html to a file. '''
    global output_filename
    with open(output_filename, "w") as f:
        global html
        f.writelines(html)


#### Main
if converter=="mammoth":
    print("if")
    readStyles()
    convertMammoth()
else:
    print("else")
    convertPandoc()

cookSoup()
writeOutput()
